/*! @mainpage Proyecto MIT
 *
 * \section genDesc General Description
 *
 * Esta aplicación mide la temperatura y humedad, y los envia via bluetooth
 *
 * \section hardConn Hardware Connection
 *
 * | 	HC05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Rx		 	| 	Tx			|
 * | 	Tx		 	| 	Rx			|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *  *
 * | 	LM35		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Data	 	| 	CH1			|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 * | 	DHT11		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Data	 	| 	GPIO1		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Justo Muñoz
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ProyectoMIT.h"       /* <= own header */
#include "delay.h"
#include "gpio.h"
#include "systemclock.h"
#include "uart.h"
#include "analog_io.h"
#include "sapi_dht11.h"
#include "HC05.h"
#include "timer.h"
#include "LM35.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint8_t humedad;
uint8_t temperatura;
uint16_t temperatura_LM35;

bool estado=false;

void DHT11(void);
void HC05HT(void);
void LM35(void);
void Check(void);
timer_config temporizador = {TIMER_A,1000,&Check};

/*==================[internal functions declaration]=========================*/
void HC05H(void){

	HC05EnviaDato(&humedad);
	HC05SendCaracter();

}
void HC05T(void){

	HC05EnviaDato(&temperatura_LM35);
	HC05SendCaracter();

}

void DHT11(void){
	dht11Read(&humedad,&temperatura);

	HC05H();
}
void LM35(void){

	temperatura_LM35= MedirTemperatura();

	HC05T();

}
void Check(void){
	estado=true;
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	dht11Init(GPIO_1);
	HC05init();
	LM35Init();

	TimerInit(&temporizador);
	TimerStart(TIMER_A);


	while(1){

		if((estado==true)){
					DHT11();

					LM35();
			estado=false;
	}

	}
    
	return 0;
}

/*==================[end of file]============================================*/

