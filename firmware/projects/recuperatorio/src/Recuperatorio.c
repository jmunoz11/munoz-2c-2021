/*! @mainpage Recuperatorio
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de a partir de 2 cintas transportadoras y una balanza, permitir el llenado de cajas en serie hasta llegar a los 20kg de contenido cada caja.
 *
 * \section hardConn Hardware Connection
 *
 * |   Infrarrojo	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Balanza 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT		 	| 	 CH1	    |
 * | 	+3.3V 	 	| 	  +3.3V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Justo Muñoz
 *

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_3_Infrarrojo.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "balanza.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

bool cinta1state=true; /* <= Estado inicial de la cinta 1 */
bool cinta2state=false; /* <= Estado inicial de la cinta 2 */
bool stop;  /* <= Booliano de control */
uint16_t cajas=0;  /* <= Contador de cajas */
uint16_t peso;  /* <= Variable que guarda el peso */
uint16_t segundos=0;  /* <= Variable que me guarda el tiempo en segundos */
uint16_t tmax=0;/* <= Variable que me guarda el tiempo max en segundos */
uint16_t tmin=0;/* <= Variable que me guarda el tiempo min en segundos */

/*==================[internal functions declaration]=========================*/
void SisInit(void);// iniciacion del sistema y periferico
void VoltToKg(void);// conversion de volt a kg
void Cinta1(void);// controla la cinta 1
Void Cinta2(void);// controla la cinta 2
void funcionamiento(void);// se encarga del funcionamiento en general
void TimepodeLlenado(void);

/*==================[external data definition]===============================*/
//timer_config my_timer = {TIMER_A,500,&VoltToKg};//]Timer para ir midiendo el peso casa 0.5 segundos
timer_config MY_TIMMER = {TIMER_A, 1000, &funcionamiento};


/*==================[external functions definition]==========================*/
void VoltToKg(void){

	float volt=0;
	volt= BalanzaRead();
	peso=volt/22;
	//SE SUPONE QUE EL PESO DE LAS CAJAS ES DESPRECIABLE
}

void Cinta1(void){
	if(cinta1state==true){
	LedOn(LED_1);}
	else{
		LedOff(LED_1);
	}
}

void Cinta2(void){
	if(cinta1state==true){
	LedOn(LED_2);}
	else{
		LedOff(LED_2);
	}
}

void funcionamiento(void){
	if(detener==false){//control para los switches
		VolTtoKg();//se llama a la funcion para actulizar el peso
	if(peso=0){//si el peso es 0
		Cinta1();
		if(Tcrt5000State()== true){ //detecta que llego a la cinta 2
			cinta1state=false;//desactiva cinta 1
			cinta2state=true;//activa cinta 2
			segundos=segundos+1;}
			else{
				if(peso=20){ // corrobora que se llego al peso
					cinta2state=false;//apaga la cinta 2
					cajas=cajas+1;//aumenta el numero de cajas y imprime por pantalla
					UartSendString(SERIAL_PORT_PC, "Tiempo de llenado de caja ");
					UartSendString(SERIAL_PORT_PC,UartItoa(cajas,10));
					UartSendString(SERIAL_PORT_PC, " ");
					UartSendString(SERIAL_PORT_PC,UartItoa(segundos,10));
					UartSendString(SERIAL_PORT_PC, "seg.");
					if(segundos<tmin=0){
						tmin=segundos;}// con estos dos if busco el amyor y menor
					if(segundos>tmax=0){
						tmax=segundos;}
					segundos=0;
					peso=0;}}}
				if(cajas=15){
					UartSendString(SERIAL_PORT_PC, "Tiempo de llenado maximo: ");
					UartSendString(SERIAL_PORT_PC,UartItoa(tmax,10));
					UartSendString(SERIAL_PORT_PC, "seg.");
					UartSendString(SERIAL_PORT_PC, "Tiempo de llenado minimo: ");
					UartSendString(SERIAL_PORT_PC,UartItoa(tmin,10));
					UartSendString(SERIAL_PORT_PC, "seg.");}
					cajas=0;//reseto la cantidad de cajas
				}
}


void TeclaUno(){
	//PrendoTodo
	detener= false;

}
void TeclaDos(){
	//ApagoTodo
	detener=true;

}


void SisInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	SwitchActivInt(SWITCH_1, TeclaUno);
	SwitchActivInt(SWITCH_2, TeclaDos);
	UartInit(&UART_USB);
}

int main(void){

	SisInit();

	while(1){

		/*Nada*/
	}

	return 0;

}




/*==================[end of file]============================================*/

