/*! @mainpage Recuperatorio
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de a partir de 2 cintas transportadoras y una balanza, permitir el llenado de cajas en serie hasta llegar a los 20kg de contenido cada caja.
 * \section hardConn Hardware Connection
 *
 * |   Balanza 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT		 	| 	 CH1	    |
 * | 	+3.3V 	 	| 	  +3.3V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Justo Muñoz
 *
 */

#ifndef _RECUPERATORIO_H
#define _RECUPERATORIO_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _RECUPERATORIO_H */

