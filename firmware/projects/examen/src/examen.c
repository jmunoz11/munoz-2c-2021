/*! @mainpage Examen
 *
 * \section genDesc General Description
 *
 * Esta aplicación consta de un sonar
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	T_FIL2		|
 * | 	Trigger	 	| 	T_FIL3		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 * | Dispositivo2	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Data	 	| 	CH1			|
 * | 	GND 	 	| 	GND	    	|
 * | 	+3.3V 	 	| 	+3.3V     	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Justo Muñoz
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/examen.h"       /* <= own header */
#include "delay.h"
#include "gpio.h"
#include "systemclock.h"
#include "uart.h"
#include "analog_io.h"
#include "timer.h"
#include "goniometro.h"
#include "hc_sr4.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
float voltaje;
float grado;
void PotVolt(void);
int16_t VoltToDegrees(void);
void ImprimeMenorDist(void);
void DistanceRead(void);
void ImprimeDistObjeto(void);
void IndicadorDeSentido(void);

timer_config temporizador = {TIMER_A,1000, &DistanceRead,&PotVolt};
serial_config UART_USB = {SERIAL_PORT_PC,115200};
/*==================[internal functions declaration]=========================*/


void PotVolt(void){//L
	voltaje=MedirVoltaje();
	VoltToDregrees();
}

int16_t VoltToDegrees(void){//L

	switch (voltaje)
	{
	case (voltaje=1.2):
		grado=0;
	break;
	case (voltaje=1.38):
		grado=15;
	break;
	case (voltaje=1.56):
		grado=30;
	break;
	case (voltaje=1.74):
		grado=45;
	break;
	case (voltaje=1.92):
		grado=60;
	break;
	case (voltaje=2.1):
		grado=70;
	break;
	}}

void DistanceRead(void){//L

	uint8_t distancia=0;
	distancia=HcSr04ReadDistanceCentimeters();
	ImprimeDistObj();
	uint16_t gradomenor;
	uint16_t menordist=100;
	if(menordist>distancia){
			distmenor=distancia0;
			gradomenor=grado;}
		else{
			distmenor=distancia;
			gradomenor=grado;}
		if((grado==75)||(grado==0)){
			ImprimeMenorDist();
		}
}

void ImprimeMenorDist(void){//L
	UartSendString(SERIAL_PORT_PC,"Obstaculo en ");
	UartSendString(SERIAL_PORT_PC,gradomenor);
	UartSendString(SERIAL_PORT_PC,"°");

}


void ImprimeDistObj(void){//L

	UartSendString(SERIAL_PORT_PC,grado);
	UartSendString(SERIAL_PORT_PC,"° objeto a");
	UartSendString(SERIAL_PORT_PC,distancia);
	UartSendString(SERIAL_PORT_PC,"cm");
}

void IndicadorDeSentido(void){//L

	uint16_t gradomenor=0;
	uint16_t gradolimite=75;
		if(grado>=gradomenor){
			gradomenor=grado;
			LedOn(LED_RGB_R);
		}
			else{
				if(gradomenor=gradolimite){
				gradomenor=grado;
				LedOn(LED_RGB_G);}
}}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	PotInit();
	LedsInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	TimerInit(&temporizador);
	TimerStart(TIMER_A);

	while(1){

		IndicadorDeSentido();


	}

	return 0;
}

/*==================[end of file]============================================*/
