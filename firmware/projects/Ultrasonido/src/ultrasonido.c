/*! @mainpage Ultrasonido
 *
 * \section genDesc General Description
 *
 * Esta aplicación mide la distancia con el uso de un ultrasonido
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	T_FIL2		|
 * | 	Trigger	 	| 	T_FIL3		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Justo Muñoz
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ultrasonido.h"       /* <= own header */
#include "delay.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "systemclock.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	int16_t distanciacm=0;

	while(1){

    	distanciacm=HcSr04ReadDistanceCentimeters();

	}
    
	return 0;
}

/*==================[end of file]============================================*/

