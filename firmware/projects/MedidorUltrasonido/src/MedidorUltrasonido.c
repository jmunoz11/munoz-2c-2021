/*! @mainpage MEDIDOR ULTRASONIDO
 *
 * \section genDesc General Description
 *
 * Esta aplicación tendra como fin medir una distancia a partir de un medidor ultrasónico y mostrar en la placa el resultado (prendiendo los Leds)
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	T_FIL2		|
 * | 	Trigger	 	| 	T_FIL3		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Justo Muñoz
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/MedidorUltrasonido.h"       /* <= own header */
#include "led.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	LedsInit();
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SwitchesInit();
	uint8_t teclas;
	uint8_t hold=0;
	uint8_t prende=0;
	uint8_t distancia=0;

	while(1){
		teclas  = SwitchesRead();
		switch(teclas){
		case SWITCH_1:
			prende=!prende;
		case SWITCH_2:
			hold=!hold;}

		if((prende)){
			distancia=HcSr04ReadDistanceCentimeters();
			if(hold){
				if(distancia<10){
					LedOn(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				else{
					if((distancia>10)&&(distancia<20)){
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					else{
						if((distancia>20)&&(distancia<30)){
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOn(LED_2);
							LedOff(LED_3);}
						else{
							if((distancia>30)){
								LedOn(LED_RGB_B);
								LedOn(LED_1);
								LedOn(LED_2);
								LedOn(LED_3);
							}}}}}}

	}


	return 0;
}

/*==================[end of file]============================================*/

