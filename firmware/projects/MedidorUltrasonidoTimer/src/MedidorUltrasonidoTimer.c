/*! @mainpage MEDIDOR ULTRASONIDO
 *
 * \section genDesc General Description
 *
 * Esta aplicación tendra como fin medir una distancia a partir de un medidor ultrasónico y mostrar en la placa el resultado (prendiendo los Leds)
 * Se agrega el uso de un timer y interrupciones
 * PROYECTO 3
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	T_FIL2		|
 * | 	Trigger	 	| 	T_FIL3		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Justo Muñoz
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/MedidorUltrasonidoTimer.h"       /* <= own header */
#include "led.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "switch.h"
#include "chip.h"
#include "timer.h"
#include "uart.h"



/*==================[macros and definitions]=================================*/
#define Tiempo 200
bool teclaprende;
bool teclahold;

uint8_t datorecibido;

bool tecla1();
bool tecla2();



/*==================[internal data definition]===============================*/
void Medicion();
void MedirTeclado();
timer_config temporizador = {TIMER_A, 500, &Medicion};



/*==================[internal functions declaration]=========================*/
bool tecla1(){
	teclaprende=!teclaprende;
		return true;
}
bool tecla2(){
	teclahold=!teclahold;
	return true;
}

void Medicion(){
	uint8_t distancia=0;
	if((teclaprende)){
		distancia=HcSr04ReadDistanceCentimeters();
		if(teclahold){
			if(distancia<10){
				LedOn(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			else{
				if((distancia>10)&&(distancia<20)){
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				else{
					if((distancia>20)&&(distancia<30)){
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);}
					else{
						if((distancia>30)){
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOn(LED_2);
							LedOn(LED_3);
						}}}}}}
}
void MedirTeclado(){
	 UartReadByte(SERIAL_PORT_PC, &datorecibido);
	 switch(datorecibido){
	 	case 'O':
	 			tecla1();
	 			break;
	 	case 'H':
	 			tecla2();
	 			break;}

};
/*==================[external data definition]===============================*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,MedirTeclado};

/*==================[external functions definition]==========================*/

int main(void){

	LedsInit();
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SwitchesInit();
	TimerInit(&temporizador);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
	UartSendString(SERIAL_PORT_PC, "HOLA JUAN");
	//SwitchActivInt(SWITCH_1, tecla1);
	//SwitchActivInt(SWITCH_2, tecla2);



		while(1){


			/*if((prende)){
			distancia=HcSr04ReadDistanceCentimeters();
			if(hold){
				if(distancia<10){
					LedOn(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				else{
					if((distancia>10)&&(distancia<20)){
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					else{
						if((distancia>20)&&(distancia<30)){
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOn(LED_2);
							LedOff(LED_3);}
						else{
							if((distancia>30)){
								LedOn(LED_RGB_B);
								LedOn(LED_1);
								LedOn(LED_2);
								LedOn(LED_3);
							}}}}}}*/

		}


		return 0;
	}

	/*==================[end of file]============================================*/

