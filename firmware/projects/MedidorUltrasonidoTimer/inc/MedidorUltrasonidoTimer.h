/*! @mainpage MEDIDORULTRASONIDO
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *Esta aplicación tendra como fin medir una distancia a partir de un medidor ultrasónico y mostrar en la placa el resultado (prendiendo los Leds)
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	T_FIL2		|
 * | 	Trigger	 	| 	T_FIL3		|
 * | 	GND 	 	| 	GND	    	|
 * | 	+5V 	 	| 	+5V     	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * | 15/09/2021	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Justo Muñoz
 *
 */

#ifndef MEDIDORULTRASONIDOTIMER_H
#define MEDIDORULTRASONIDOTIMER_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef MEDIDORULTRASONIDOTIMER_H */

