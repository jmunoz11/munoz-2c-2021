/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * 2do Cuatrimestre 2021
 * Autor: Justo Muñoz
 *
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

union valor32bits{
	struct variables{
	uint8_t variable11;
	uint8_t variable22;
	uint8_t variable33;
	uint8_t variable44;
	}variable8bits;
	uint32_t valorinicial;
}launion;



/*==================[internal functions declaration]=========================*/

int main(void)
{

	launion.valorinicial=0x01020304;
	printf("%d\n",launion.valorinicial);
	launion.variable8bits.variable44=launion.valorinicial;//04
	launion.variable8bits.variable33=(launion.valorinicial>>8);//03
	launion.variable8bits.variable22=(launion.valorinicial>>16);//02
	launion.variable8bits.variable11=(launion.valorinicial>>24);//01

	printf("%d\n",launion.valorinicial);
	printf("%d\n",launion.variable8bits.variable11);
	printf("%d\n",launion.variable8bits.variable22);
	printf("%d\n",launion.variable8bits.variable33);
	printf("%d\n",launion.variable8bits.variable44);


	return 0;
}

/*==================[end of file]============================================*/

