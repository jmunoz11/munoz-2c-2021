/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * 2do Cuatrimestre 2021
 * Autor: Justo Muñoz
 *
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
typedef struct{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;
}gpioConf_t;

gpioConf_t arreglo [4]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};

/*==================[internal functions declaration]=========================*/

void SEG7(gpioConf_t * arreglo, uint8_t bcd){
int i;
	for(i=0;i<4;i++){
		uint8_t mascara1=(1<<i);
		if(mascara1 & bcd){
			printf("Prende el puerto %d",arreglo[i].port);
			printf(" en el pin %d\n",arreglo[i].pin);

		}
		else{
			printf("Apaga el puerto %d",arreglo[i].port);
			printf(" en el pin %d\n",arreglo[i].pin);}}
		};

int8_t vector[4];
int8_t i;
void BinaryToBcd(uint32_t data, uint8_t digits,uint8_t *bcd_number){
	int8_t aux1,aux2;
	for(i=0;i<digits;i++){
		aux1=data%10;
		vector[i]=aux1;
		aux2=data/10;
		data=aux2;
	}
	for(i=digits-1;i>=0;i--){
		printf("Digito:%u\n", vector[i]);
		SEG7(arreglo,vector[i]);
	}
}

int main(void)
{
	BinaryToBcd(1234,4,vector);
	//SEG7(arreglo,9);

	return 0;
}

/*==================[end of file]============================================*/

