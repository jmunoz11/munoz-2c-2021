/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * 2do Cuatrimestre 2021
 * Autor: Justo Muñoz
 *
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/
struct alumno{
	char apellido[20];
	char nombre[12];
	uint8_t edad;
}alumno1, alumnoM2, *alumno2;



/*==================[internal functions declaration]=========================*/

int main(void)
{
	alumno2=&alumnoM2;
	strcpy(alumno1.nombre,"justo");
	strcpy(alumno1.apellido,"Munoz");
	alumno1.edad=22;

	strcpy(alumno2->nombre,"Clementina");
	strcpy(alumno2->apellido,"Munoz");
	alumno2->edad=21;

	printf("Nombre: %s\n", alumno1.nombre);
	printf("Apellido: %s\n", alumno1.apellido);
	printf("Edad: %d\n", alumno1.edad);

	printf("Nombre: %s\n", alumno2->nombre);
	printf("Apellido: %s\n", alumno2->apellido);
	printf("Edad: %d\n", alumno2->edad);
	return 0;
}

/*==================[end of file]============================================*/

