/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * 2do Cuatrimestre 2021
 * Autor: Justo Muñoz
 *
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
typedef struct{
	uint8_t n_led;
	uint8_t n_ciclos;
	uint8_t periodo;
	uint8_t mode;
}leds;

typedef enum {On,Off,Toogle} modo_t;// on es 1, off es 2 y toogle es 3
typedef enum {Led1=1,Led2=2,Led3=3} led_t;

/*==================[internal functions declaration]=========================*/

void Leds(leds * led){
	uint8_t i,j;
	switch(led->mode){
	case On:// es lo mismo que poner Case 0 pq ya lo definimos
		switch (led->n_led){
		case Led1:
			printf("Se prende Led %u",led->n_led);
			break;
		case Led2:
			printf("Se prende Led %u",led->n_led);
			break;
		case Led3:
			printf("Se prende Led %u",led->n_led);
			break;}
		break;
		case Off:
			switch (led->n_led){
			case Led1:
				printf("Se apaga Led %u",led->n_led);
				break;
			case Led2:
				printf("Se apaga Led %u",led->n_led);
				break;
			case Led3:
				printf("Se apaga Led %u",led->n_led);
				break;}
			break;
		case Toogle:
				for(i=0;i<led->n_ciclos;i++){
			switch (led->n_led){
				case Led1:
					printf("Se togglea Led %u\n",led->n_led);
				break;
				case Led2:
					printf("Se togglea Led %u\n",led->n_led);
				break;
				case Led3:
					printf("Se togglea Led %u\n",led->n_led);
				break;}

				for(j=0;j<led->periodo;j++){
									printf("Toogleando...\n");
								}}
				break;

	}
}


int main(void)
{

	leds l = {Led1,3,3,Toogle};
	Leds (&l);



	return 0;
}

/*==================[end of file]============================================*/

